% !TEX root = presentation.tex

\section{Intérêt et applications de git}

\begin{frame}\frametitle{Pourquoi utiliser un suivi de version ?}


\begin{itemize}[label={$\bullet$}]
\item \bblue{Enregistrer} les modifications d'un jeu de fichiers au cours du temps
\item Rester \bblue{réversible} :
    \begin{itemize}[label={$-$}]
    \item pouvoir retourner à une version antérieure,
    \item comparer avec une version antérieure
    \end{itemize}
\item \bblue{Documenter} les modifications (date, auteur et message d'accompagnement)
\item Un logiciel de suivi de version (VCS pour \emph{Version Control System} en anglais) comme Git gère très bien tout projet qui se présente sous la forme de \bblue{fichiers sources}
\item C'est le meilleur moyen de \bblue{collaborer} sur des sources !

\end{itemize}

\end{frame}


\begin{frame}\frametitle{Pourquoi utiliser un suivi de version ?}

\begin{figure}
\includegraphics[width=0.15\textwidth]{Fig/git/git-logo.png} \hspace*{0.85\textwidth}
\end{figure}

\vspace{5mm}

\begin{columns}

\column{0.45\textwidth}
\bblue{Ce que Git gère très bien :}
\begin{itemize}[label={\cmark}]
\item les scripts ou code de calcul
\item les documents \LaTeX\ (cet exposé est sur \href{https://gitlab.math.unistra.fr/formation-info/expose-git}{\color{magenta} GitLab} !)
\item les fichiers texte de configuration
\item les sources html
\item etc.
\end{itemize}

\column{0.5\textwidth}
\bblue{Ce que Git gère mal :}
\begin{itemize}[label={\xmark}]
\item les gros fichiers binaires
\item les documents Microsoft Office ou OpenOffice
\item le texte formaté en général
\item les bases de données (type mysql)
\item etc.
\end{itemize}

\end{columns}

\end{frame}

\begin{frame}\frametitle{Comparaison avec un système de partage de type Owncloud ou Seafile}
\begin{tabular}{l p{35mm} | p{45mm}}
 & \textbf{Owncloud/Seafile} & \textbf{Git} \\
\hline
\bblue{Type de fichiers} & \cmark tous types & \cmark suivi pour les fichiers sources \\
 & & \xmark pas de suivi pour les binaires \\
& & \xmark pas adapté aux gros fichiers (sauf avec git-lfs) \\
\hline
\bblue{Suivi de version} & \xmark très limité & \cmark outil avancé \\
\hline
\bblue{Partage} &  \xmark modèle centralisé uniquement & \cmark modèle distribué  \\
& \xmark synchronisations essentiellement automatiques & \cmark on contrôle les synchronisations \\
\hline
\bblue{Prise en main} &  \cmark très simple & \xmark demande un apprentissage
\end{tabular}
\end{frame}


{
\setbeamertemplate{caption}{\centering\insertcaption\par}
\begin{frame}
\frametitle{Un suivi de version distribué}

\begin{itemize}[label={$\bullet$}]
\item les clients possèdent un miroir complet de la base de données du serveur
\item on peut travailler en mode déconnecté et synchroniser quand on le souhaite
\item indirectement, on crée des sauvegardes multiples
\end{itemize}

\begin{figure}
\includegraphics[width=0.35\textwidth]{Fig/git/distributed.png}
\caption*{\scriptsize{(Source: Pro Git book \url{http://git-scm.com/book})}}
\end{figure}

\end{frame}
}

{
\setbeamertemplate{caption}{\centering\insertcaption\par}
\begin{frame}[fragile]
\frametitle{Git en pratique}


\begin{columns}
\column{0.5\textwidth}
Git en ligne de commande dans le Terminal :
\vspace{5mm}
\begin{lstlisting}[language=bash]
git add
git commit -m "My commit message"
git status
git log
git push
git pull
git checkout
git diff
etc.
\end{lstlisting}

\column{0.5\textwidth}

\begin{figure}
\includegraphics[width=1.1\textwidth]{Fig/git/git-commands.png}
\end{figure}
\end{columns}

\end{frame}
}

{
\setbeamertemplate{caption}{\centering\insertcaption\par}
\begin{frame}[fragile]
\frametitle{Git en pratique}

Avec une interface graphique très simple comme  \href{https://desktop.github.com/}{\color{magenta} GitHub Desktop}, on couvre $\approx 90\%$ de l'utilisation courante de git :

\begin{figure}
\includegraphics[width=0.9\textwidth]{Fig/git/githubdesktop.png}
\end{figure}


\end{frame}
}


{
\setbeamertemplate{caption}{\centering\insertcaption\par}
\begin{frame}
\frametitle{Les quatre statuts des fichiers suivis}

\begin{itemize}[label={$\bullet$}]

\item Le cycle de vie d'un fichier suivi avec Git

\begin{figure}
\includegraphics[width=0.9\textwidth]{Fig/git/lifecycle.png}
\caption*{\scriptsize{(Source: Pro Git book \url{http://git-scm.com/book})}}
\end{figure}

\item Les fichiers qui ne sont pas des sources (fichiers objets, fichiers de compilations, exécutables, etc.) peuvent être ignorés.

\end{itemize}

\end{frame}
}

{
\setbeamertemplate{caption}{\centering\insertcaption\par}
\begin{frame}
\frametitle{Git et le système des branches}


\begin{itemize}[label={$\bullet$}]
\item Git permet de créer et fusionner très facilement des branches
\item un système de branches permet de préserver une version stable (branche \emph{master}) sans limiter les développements (branche \emph{develop})
\item les branches sont particulièrement utiles pour le travail collaboratif et par sujet \emph{(topic)}.
\end{itemize}

\begin{figure}
\includegraphics[width=0.65\textwidth]{Fig/git/lr-branches-2.png}
\caption*{\scriptsize{(Source: Pro Git book \url{http://git-scm.com/book})}}
\end{figure}

\end{frame}
}

{
\setbeamertemplate{caption}{\centering\insertcaption\par}

\begin{frame}
\frametitle{Travail collaboratif avec Git : le workflow typique d'une petite équipe}

\begin{columns}

\column{0.6\textwidth}

\begin{figure}
\includegraphics[width=0.8\textwidth]{Fig/git/small-team-flow.png}
\caption*{\scriptsize{(Source: Pro Git book \url{http://git-scm.com/book})}}
\end{figure}

\column{0.45\textwidth}

En pratique :
\begin{itemize}[label={$\bullet$}]
    \item Côté serveur : \bblue{GitLab}
    \item Côté clients (John et Jessica) : ligne de commande ou client graphique (GitHub Desktop, par exemple)
\end{itemize}

\end{columns}

\end{frame}
}

\begin{frame}
\frametitle{Travail collaboratif avec Git}

\begin{itemize}[label={$\bullet$}]
    \item Même si \bblue{Git} est basé sur un modèle distribué, le partage se fait essentiellement via un \bblue{serveur Git.}
\end{itemize}

\begin{block}{Comparaison des services d'hébergement Git en version gratuite}

\begin{columns}[onlytextwidth]
\begin{column}{0.3\textwidth}
\centering
\textbf{Github}
\end{column}

\begin{column}{0.3\textwidth}
\centering
\textbf{Bitbucket}
\end{column}

\begin{column}{0.3\textwidth}
\centering
\textbf{GitLab}
\end{column}
​
\end{columns}

\begin{columns}[onlytextwidth]

\begin{column}{0.3\textwidth}
\begin{itemize}
\item[\cmark] très gros projet (73+ millions dépôts, 718 employés) \href{https://github.com/about}{\scriptsize [Ref.]}
\item[\cmark] Grande interopérabilité avec d'autres outils 
\item[\xmark] dépôts publics uniquement
\item[\xmark] pas d'instance privée
\end{itemize}
\end{column}

\begin{column}{0.3\textwidth}
\begin{itemize}
\item[\cmark] très gros projet
\item[\cmark] Git \& Mercurial
\item[\cmark] intégration dans les produits Atlassian
\item[\xmark] 5 utilisateurs max/dépôt
\item[\xmark] pas d'instance privée
\end{itemize}
\end{column}

\begin{column}{0.3\textwidth}
\begin{itemize}
\item[\cmark] projet pérenne (204 employés, 39 pays) \href{https://about.gitlab.com/team}{\scriptsize [Ref.]}
\item[\cmark] peu de limites dans la version hébergée
\item[\xmark] certaines limitations en taille
\item[\cmark] instance privée opensource
\item[\cmark] outils d'intégration continue natifs
\end{itemize}
\end{column}
​
\end{columns}

{\scriptsize (décembre 2017, d'après \url{http://comparegithosting.com})}

\end{block}


Parmi ces 3 géants, \bblue{GitLab} est la seule solution qui permet d'héberger une instance privée (problème de l'hébergement des données de recherche sur des clouds privés).

%\footnotesize{\hfill D'autres éléments de comparaison sur \url{http://comparegithosting.com}.}

\end{frame}

%\begin{frame}
%\frametitle{Travail collaboratif avec Git}
%
%
%Des solutions institutionnelles existent comme \url{https://gitlab.unistra.fr} alors \bblue{pourquoi déployer un serveur GitLab local ?}
%
%\begin{itemize}[label={$\bullet$}]
%\item pour maîtriser l'accès au service (en particulier pour les collaborateurs extérieurs)
%\item pour maîtriser la maintenance (mise à jour, ajout de fonctionnalités, etc.)
%\item besoin faible en ressources informatiques
%\item effort modéré pour l'administration
%\item politiquement : on identifie le contenu à l'entité qui l'héberge
%
%\end{itemize}
%
%\end{frame}
%
