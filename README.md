# Objectifs

- pour les non initiés : montrer ce qu'on peut faire avec git et gitlab
- pour tout le monde :
	- exposer les fonctionalités de gitlab
	- décrire l'administration d'un gitlab

L'auditoire doit partir avec des réponses aux questions suivantes :

- est-ce que j'ai intérêt à utiliser Git dans mon travail ?
- est-ce que mes utilisateurs ont intérêt ?
- est-ce que j'ai intérêt à déployer un serveur GitLab dans mon unité ?

